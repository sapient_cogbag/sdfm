//! Hold context for the program in which it is running
//! Nya
use super::stateparser::err::StateBlockError;

/// Holds things like access to sudo and if we are root
#[derive(Eq, PartialEq, Copy, Clone, Debug)]
pub enum SystemPowers {
    /// Have root access
    Root,
    /// Not root access nya.
    ///
    /// Holds if we can try elevate our privileges
    No {
        can_try_elevate: bool
    } 
}

impl PartialOrd for SystemPowers {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        match (self, other) {
            (SystemPowers::Root, SystemPowers::Root) 
                => Some(std::cmp::Ordering::Equal),
            (SystemPowers::Root, SystemPowers::No {can_try_elevate: true }) 
                => Some(std::cmp::Ordering::Greater),
            (SystemPowers::Root, SystemPowers::No {can_try_elevate: false}) 
                => Some(std::cmp::Ordering::Greater),
            (SystemPowers::No {can_try_elevate: true}, SystemPowers::Root) 
                => Some(std::cmp::Ordering::Less),
            (SystemPowers::No {can_try_elevate: true}, 
             SystemPowers::No {can_try_elevate: true}) 
                => Some(std::cmp::Ordering::Equal),
            (SystemPowers::No {can_try_elevate: true}, 
             SystemPowers::No {can_try_elevate: false}) 
                => Some(std::cmp::Ordering::Greater),
            (SystemPowers::No {can_try_elevate: false}, SystemPowers::Root) 
                => Some(std::cmp::Ordering::Less),
            (SystemPowers::No {can_try_elevate: false}, 
             SystemPowers::No {can_try_elevate: true}) 
                => Some(std::cmp::Ordering::Less),
            (SystemPowers::No {can_try_elevate: false}, 
             SystemPowers::No {can_try_elevate: false}) 
                => Some(std::cmp::Ordering::Equal)
        }
    }
}

impl Ord for SystemPowers {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.partial_cmp(other).unwrap()
    }
}




/// Represents an action within a given system context nyaaa 
pub trait Action <T> {
    /// Tests if a context can actually allow this action
    /// to occur (does not guaruntee success of course nya)
    fn appliable_in_context(&self, ctx: &impl Context) -> bool;

    /// Actually perform the action in the given context.
    fn perform(self, ctx: &impl Context) -> Result::<T, StateBlockError>;

    
}


/// An interface for actions that may have alternatives to get
/// systemwide configuration into per-user configuration nya
pub trait DesystemisableAction <T>: Action::<T> {
    type Desystemised: Action::<T>;
    /// Try to de-systemise an action - in particular this 
    /// allows for actions which change the system in one way
    /// but focus into per-user configuration if possible nya
    ///
    /// In the cases where it is not possible it should return a None
    fn de_systemise(self) -> Option::<Self::Desystemised>;
}




/// Type indicating the context in which the program is running, in relation to things
/// like access to root and different relevant paths nya
pub trait Context {
    /// Get the current contextual powers nya
    fn power(&self) -> SystemPowers;

}




// sdfm
// Copyright (C) 2020 sapient_cogbag <sapient_cogbag at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
