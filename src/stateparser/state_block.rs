//! Holds traits of state blocks. State blocks 
//! hold system configuration states that can be applied in 
//! any order - for instance a collection of packages nya
//!
//! Scripts themselves are likely to be singularly whole state blocks
//! nyaa
//!
//! State blocks also allow for a nice tag type so you can build different
//! systems of states
use super::err;

pub trait StateBlock <State> {
    /// A simple tag type for holding states 
    type Tag;
}

// sdfm
// Copyright (C) 2020 sapient_cogbag <sapient_cogbag at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
