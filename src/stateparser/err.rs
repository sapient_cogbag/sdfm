
/// Holds some useful details about sources of errors nya
#[derive(Clone, Debug)]
pub struct Details {
    /// The file that produced the error nyaa
    source_file: Option::<String>,
    /// An error message nya
    error_message: Option::<String>,
}

impl std::fmt::Display for Details {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        if let Some(src) = &self.source_file {
            writeln!(f, "From file: {}", src)?;
        }
        if let Some(msg) = &self.error_message {
            writeln!(f, "Error Message nya:\n{}", msg)?;
        }
        Ok(())
    }
}

#[derive(Clone, Debug)] 
/// Holds an error from applying a state-block or state nyaa
pub enum StateBlockError {
    /// Indicates that a process crashed nya
    ProcessCrash {
        /// The error code produced by the process
        process_error_code: Option::<isize>,
        /// The process (and arguments) that were executed nyaaa
        executed_command: Option::<String>,
        details: Details
    },
    /// Error in starting a process nya
    ProcessStartError {
        /// The command that was attempted to start:
        failed_command: Option::<String>,
        details: Details
    },
    /// An other error nya
    OtherError {
        details: Details
    }
}

type SResult<T> = Result::<T, StateBlockError>;

impl std::fmt::Display for StateBlockError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            StateBlockError::ProcessCrash { details, process_error_code, executed_command} => {
                writeln!(f, "Process crashed. Details:")?;
                writeln!(f, "{}", details)?;
                if let Some(errc) = process_error_code {
                    writeln!(f, "Process error return code nya: {}", errc)?;
                }
                if let Some(exec) = executed_command {
                    writeln!(f, "Executed command: {}", exec)?;
                }
            },
            StateBlockError::ProcessStartError { details, failed_command } => {
                writeln!(f, "Could not start process. Details:")?;
                writeln!(f, "{}", details)?;
                if let Some(failed) = failed_command {
                    writeln!(f, "Command that failed to run nya: {}", failed)?;
                }
            }
            StateBlockError::OtherError { details } => {
                writeln!(f, "Unspecified Error")?;
                writeln!(f, "{}", details)?;
            }
        }
        Ok(())
    }
}

impl std::error::Error for StateBlockError {}


// sdfm
// Copyright (C) 2020 sapient_cogbag <sapient_cogbag at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
