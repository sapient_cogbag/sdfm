# sdfm

System and Dotfile Manager - Arch-focused system and dotfile manager/creator nya

### Goals
This programming system is designed for four main goals nya:
* Managing user dotfiles in a reasonably modular manner, and not needing to manage dotfiles you don't care about
* Managing configuration on systems you both do and do not control in an automated efficient manner: 
  * Being able to create a new system on a drive with minimal downloaded binaries (ideally just the SDFM binary) in an 
    automated manner nya, that allows efficient cloning of system configuration that the user specified they care about 
    for instance relevant services being enabled or having configuration grabbed by a single command once you first     
    start up
  * Efficiently managing systemwide and per-user vim plugins and similar things - Arch and the AUR in particular 
    hold vim plugins and for some people (the original author of this program included nya), it is preferred to manage
    things with the package manager where possible (i.e. when you have `sudo` access to the system nya), so this   
    software should provide good mechanisms for fallback configuration of things like themes and vim plugins depending  
    on if you have sysadmin powers or not nya
  * Utilising custom packages (e.g. user PKGBUILDs not necessarily on the AUR) if the user has those in a relatively  
    automated fashion, along with regenerating those nya.
* Preventing accidental leaks of information in the form of usernames in filepaths nya, as well as resilience to minor  
  changes in config file format:
  * This means that files which include user paths inside them should not need to be stored directly in the     
    repository, instead being controlled by parameterised files (for instance, parameterised on $USER or $HOME) that    
    are generated. nya
  * For config files or service files that are likely to have changes in ordering or occasional added components or 
    updates or fixes that would break configuration if you used a fixed file, where you only care about one specific    
    component like adding an entry to a list in systemd unit files to fix an issue with permissions, you should be able 
    to specify this. 
  * The idea here is some kind of "ensures" file that specifies line patterns and entry tokeniser 
    rules for the purposes of ensuring a given configuration is some value nya, which allows generality and avoids being
    broken by changes unless they directly affect the setting you want to change nyaa
* Ensuring that if a configuration specification breaks, it can be fixed and resumed where possible during an install.
  This is extremely important, for instance, when you are in an install disk and need to change something - sdfm should 
  try to allow efficient resuming on modification and make a good attempt at tracking which actions have been completed 
  nya. 

### Secondary Goals
This programming system should try to be generic enough that it can be ported to other systems, even without systemd or that don't necessarily use the `pacman` package manager nya. This is a secondary goal because this system is developed by someone who uses arch-based systems - however it is very important to work at least for plain old user configuration files like vim plugins can be.